﻿using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public GameObject player;
    public float smoothing;
    public Vector2 maxPosition;
    public Vector2 minPosition;

    private Vector3 offset;            

    // Use this for initialization
    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    
    void LateUpdate()
    {
        if  (transform.position != player.transform.position)
        {
            Vector3 targetPosition = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);

            targetPosition.x = Mathf.Clamp(targetPosition.x, minPosition.x, maxPosition.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, minPosition.y, maxPosition.y);

            transform.position = Vector3.Lerp(transform.position, targetPosition + offset, smoothing);
        }
    }
}
