﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
	public static void SaveGame (GameManager gameManager) 
	{
        Debug.Log("Saving");

        BinaryFormatter formatter = new BinaryFormatter();
		
		string path = Application.persistentDataPath +  "/game.sav";
		FileStream stream = new FileStream(path, FileMode.Create);

        GameData data = new GameData(gameManager);
		
		formatter.Serialize(stream, data);
		stream.Close();

        Debug.Log("Save Game: Game data has been saved!");
    }
	
	public static GameData LoadGame () 
	{
		string path = Application.persistentDataPath + "/game.sav";
		if (File.Exists(path)) 
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(path, FileMode.Open);

            GameData data = formatter.Deserialize(stream) as GameData;
			stream.Close();

            Debug.Log("Save Game: Game data has been loaded!");

            return data;
		} else 
		{
			Debug.LogError("Save file not found in " + path);
			return null;
		}
	}
}