﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
	public int health;
	public string currentScene;
	//public float[] position;
	
	public GameData(GameManager gameManager)
	{
		health = gameManager.health;

		currentScene = gameManager.currentScene;

		//position = new float[3];
		//position[0] = gameManager.position.x;
		//position[1] = gameManager.position.y;
		//position[2] = gameManager.position.z;
	}
}