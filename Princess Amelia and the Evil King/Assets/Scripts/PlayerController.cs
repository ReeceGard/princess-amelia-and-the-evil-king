﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance { set; get; }

    public int health;
    public float speed = 5f;

    [SerializeField] Rigidbody2D rb;
    [SerializeField] Animator animator;

    Vector2 movement;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        if (instance == null)
        {
            instance = this;
        }

        //gameObject.transform.position = GameManager.instance.position;
    }

    // Update is called once per frame
    private void Update()
    {
        movement = Vector2.zero;
        movement.x = SimpleInput.GetAxis("Horizontal");
        movement.y = SimpleInput.GetAxis("Vertical");
        if(movement != Vector2.zero)
        {
            animator.SetFloat("MoveX", movement.x);
            animator.SetFloat("MoveY", movement.y);
        }

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * speed * Time.fixedDeltaTime);
        //GameManager.instance.position = gameObject.transform.position;
    }
}
