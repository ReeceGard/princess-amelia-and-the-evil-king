﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup : MonoBehaviour
{
    public int maxValue;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            CurrencyManager.instance.coinAmount += Random.Range(1, maxValue);
            Debug.Log("You have " + CurrencyManager.instance.coinAmount + " coins");
            HUDManager.instance.UpdateHUD();
            Destroy(gameObject);
        }
    }
}
