﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIManager : MonoBehaviour
{
    public void NewGame()
    {
        SceneManager.LoadScene(GameManager.instance.firstScene);
    }

    public void SaveGame()
    {
        GameManager.instance.SaveGame();
    }

    public void LoadGame()
    {
        GameManager.instance.LoadGame();
    }

    public void WildlifeCon()
    {
        Application.OpenURL("https://www.nationalgeographic.org/encyclopedia/wildlife-conservation/");
    }
}
