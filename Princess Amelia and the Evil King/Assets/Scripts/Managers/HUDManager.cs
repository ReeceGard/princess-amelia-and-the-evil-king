﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUDManager : MonoBehaviour
{
    public static HUDManager instance { set; get; }

    public Text coinText;
    public string mainMenuLevel;

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

        coinText.text = "Coins: " + CurrencyManager.instance.coinAmount.ToString();
    }

    public void UpdateHUD()
    {
        coinText.text = "Coins: " + CurrencyManager.instance.coinAmount.ToString();
    }

    public void SaveGame()
    {
        GameManager.instance.SaveGame();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(mainMenuLevel);
    }
}
