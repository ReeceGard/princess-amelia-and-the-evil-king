﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { set; get; }

    public string firstScene;
    public string gameOverScene;
    public string currentScene;
    public int health;
    public Vector3 position;

    // Start is called before the first frame update
    public void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance == null)
        {
            instance = this;
        }
    }

    public void SaveGame()
    {
        SaveSystem.SaveGame(this);
        Debug.Log("Game Mamager: Game data has been saved!");
        //Debug.Log("Player is at " + position);
    }

    public void LoadGame()
    {
        SceneManager.LoadScene(firstScene);

        GameData data = SaveSystem.LoadGame();

        health = data.health;

        //position.x = data.position[0];
        //position.y = data.position[1];
        //position.z = data.position[2];

        Debug.Log("Game Mamager: Game data has been loaded!");
    }

    public void GetCurrentScene()
    {
        Scene scene = SceneManager.GetActiveScene();

        currentScene = scene.name;
    }
}
